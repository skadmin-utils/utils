<?php

declare(strict_types=1);

namespace SkadminUtils\Utils\Sitemap;

use DateTimeInterface;
use Nette\Utils\DateTime;

use function in_array;

class SitemapDefinition
{
    public const CHANGEFREG_ALWAYS  = 'always';
    public const CHANGEFREG_HOURLY  = 'hourly';
    public const CHANGEFREG_DAILY   = 'daily';
    public const CHANGEFREG_WEEKLY  = 'weekly';
    public const CHANGEFREG_MONTHLY = 'monthly';
    public const CHANGEFREG_YEARLY  = 'yearly';
    public const CHANGEFREG_NEVER   = 'never';

    public const CHANGEFREG = [
        self::CHANGEFREG_ALWAYS,
        self::CHANGEFREG_HOURLY,
        self::CHANGEFREG_DAILY,
        self::CHANGEFREG_WEEKLY,
        self::CHANGEFREG_MONTHLY,
        self::CHANGEFREG_YEARLY,
        self::CHANGEFREG_NEVER,
    ];

    protected string $loc;

    protected DateTimeInterface $lastmod;

    protected string $changefreq;

    protected float $priority;

    public function __construct(string $loc, ?DateTimeInterface $lastmod = null, string $changefreq = self::CHANGEFREG_MONTHLY, float $priority = 0.8)
    {
        $this->loc      = $loc;
        $this->priority = $priority;

        $this->setLastmod($lastmod);
        $this->setChangefreq($changefreq);
    }

    public function getLoc(): string
    {
        return $this->loc;
    }

    public function setLoc(string $loc): void
    {
        $this->loc = $loc;
    }

    public function getLastmod(): DateTimeInterface
    {
        return $this->lastmod;
    }

    public function setLastmod(?DateTimeInterface $lastmod): void
    {
        if ($lastmod === null) {
            $lastmod = new DateTime();
        }

        $this->lastmod = $lastmod;
    }

    public function getChangefreq(): string
    {
        return $this->changefreq;
    }

    public function setChangefreq(string $changefreq): void
    {
        if (! in_array($changefreq, self::CHANGEFREG, true)) {
            $changefreq = self::CHANGEFREG_MONTHLY;
        }

        $this->changefreq = $changefreq;
    }

    public function getPriority(): float
    {
        return $this->priority;
    }

    public function setPriority(float $priority): void
    {
        $this->priority = $priority;
    }
}
