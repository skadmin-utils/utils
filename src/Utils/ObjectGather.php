<?php

declare(strict_types=1);


namespace SkadminUtils\Utils\Utils;

use function method_exists;
use function property_exists;

class ObjectGather
{

    public static function get(?object $object, string $property, mixed $default = null): mixed
    {
        if ($object === null) {
            return $default;
        }

        $methodGet = 'get' . ucfirst($property);
        $methodIs = 'is' . ucfirst($property);

        if (method_exists($object, $methodGet)) {
            return $object->$methodGet();
        }

        if (method_exists($object, $methodIs)) {
            return $object->$methodIs();
        }

        if (property_exists($object, $property)) {
            return $object->$property;
        }

        return $default;
    }

}
