<?php

declare(strict_types=1);

namespace SkadminUtils\Utils\Utils;

use function hexdec;
use function pow;
use function substr;

class Colors
{
    public static function getContrastColor(string $hexColor): string
    {
        // hexColor RGB
        $r1 = hexdec(substr($hexColor, 1, 2));
        $g1 = hexdec(substr($hexColor, 3, 2));
        $b1 = hexdec(substr($hexColor, 5, 2));

        // Black RGB
        $blackColor   = '#000000';
        $r2BlackColor = hexdec(substr($blackColor, 1, 2));
        $g2BlackColor = hexdec(substr($blackColor, 3, 2));
        $b2BlackColor = hexdec(substr($blackColor, 5, 2));

        // Calc contrast ratio
        $l1 = 0.2126 * pow($r1 / 255, 2.2) +
            0.7152 * pow($g1 / 255, 2.2) +
            0.0722 * pow($b1 / 255, 2.2);

        $l2 = 0.2126 * pow($r2BlackColor / 255, 2.2) +
            0.7152 * pow($g2BlackColor / 255, 2.2) +
            0.0722 * pow($b2BlackColor / 255, 2.2);

        $contrastRatio = 0;
        if ($l1 > $l2) {
            $contrastRatio = (int) ($l1 + 0.05) / ($l2 + 0.05);
        } else {
            $contrastRatio = (int) ($l2 + 0.05) / ($l1 + 0.05);
        }

        // If contrast is more than 5, return black color
        if ($contrastRatio > 5) {
            return '#000000';
        }

        // if not, return white color.
        return '#FFFFFF';
    }
}
