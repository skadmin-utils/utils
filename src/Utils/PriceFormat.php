<?php

declare(strict_types=1);

namespace SkadminUtils\Utils\Utils;

use function number_format;
use function sprintf;

class PriceFormat
{
    protected string $format;
    protected string $currency;
    protected int    $decimal;
    protected string $decimalPoint;
    protected string $thousandsSeparator;

    public function __construct(string $format = '%s %s', string $currency = 'Kč', int $decimal = 0, string $decimalPoint = ',', string $thousandsSeparator = ' ')
    {
        $this->format             = $format;
        $this->currency           = $currency;
        $this->decimal            = $decimal;
        $this->decimalPoint       = $decimalPoint;
        $this->thousandsSeparator = $thousandsSeparator;
    }

    public static function create(string $format = '%s %s', string $currency = 'Kč', int $decimal = 0, string $decimalPoint = ',', string $thousandsSeparator = ' '): self
    {
        return new static($format, $currency, $decimal, $decimalPoint, $thousandsSeparator);
    }

    public function toString(float $price): string
    {
        return sprintf($this->getFormat(), number_format($price, $this->getDecimal(), $this->getDecimalPoint(), $this->getThousandsSeparator()), $this->getCurrency());
    }

    public function getFormat(): string
    {
        return $this->format;
    }

    public function setFormat(string $format): PriceFormat
    {
        $this->format = $format;

        return $this;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): PriceFormat
    {
        $this->currency = $currency;

        return $this;
    }

    public function getDecimal(): int
    {
        return $this->decimal;
    }

    public function setDecimal(int $decimal): PriceFormat
    {
        $this->decimal = $decimal;

        return $this;
    }

    public function getDecimalPoint(): string
    {
        return $this->decimalPoint;
    }

    public function setDecimalPoint(string $decimalPoint): PriceFormat
    {
        $this->decimalPoint = $decimalPoint;

        return $this;
    }

    public function getThousandsSeparator(): string
    {
        return $this->thousandsSeparator;
    }

    public function setThousandsSeparator(string $thousandsSeparator): PriceFormat
    {
        $this->thousandsSeparator = $thousandsSeparator;

        return $this;
    }
}
