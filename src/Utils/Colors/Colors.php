<?php

declare(strict_types=1);

namespace SkadminUtils\Utils\Utils\Colors;

use JetBrains\PhpStorm\Deprecated;

#[Deprecated(reason: 'Use SkadminUtils\Utils\Utils\Colors\Colors instead.', replacement: 'SkadminUtils\Utils\Utils\Colors')]
class Colors extends \SkadminUtils\Utils\Utils\Colors
{
}
