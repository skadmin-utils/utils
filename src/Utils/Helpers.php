<?php

declare(strict_types=1);

namespace SkadminUtils\Utils\Utils;

use function intval;
use function round;
use function str_word_count;
use function strip_tags;

class Helpers
{
    public static function readTime(string $content, int $wordsPerMinute = 200): int
    {
        $pureContent = strip_tags($content);
        $words       = str_word_count($pureContent, 0, 'ěščřžýáíéůúďňóť');

        return intval(round($words / $wordsPerMinute));
    }
}
