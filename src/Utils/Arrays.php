<?php

declare(strict_types=1);

namespace SkadminUtils\Utils\Utils;

use function shuffle;

class Arrays extends \Nette\Utils\Arrays
{
    public static function shuffle(array $array): array
    {
        shuffle($array);

        return $array;
    }
}
