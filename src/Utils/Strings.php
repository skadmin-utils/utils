<?php

declare(strict_types=1);

//namespace App\Model\System;

namespace SkadminUtils\Utils\Utils;

use function array_map;
use function explode;
use function implode;
use function preg_replace;
use function strtolower;

class Strings extends \Nette\Utils\Strings
{
    public static function camelize(string $_string): string
    {
        $string = self::webalize($_string);
        $words  = explode('-', $string);
        $words  = array_map('ucfirst', $words);

        return implode('', $words);
    }

    public static function camelizeToWebalize(string $_string): string
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '-$0', $_string));
    }
}
