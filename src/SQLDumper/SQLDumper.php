<?php

declare(strict_types=1);

namespace SkadminUtils\Utils\SQLDumper;

use MySQLDump;
use mysqli;
use Nette\Utils\Random;
use Nette\Utils\Strings;
use SkadminUtils\Utils\Utils\SystemDir;

use function array_merge;
use function array_search;
use function array_unique;
use function count;
use function date;
use function implode;
use function in_array;
use function sprintf;

use const DIRECTORY_SEPARATOR;

class SQLDumper
{
    private const DEFAULT_EXCLUDES = [
        'mail_queue',
        'translation',
        'translation_mutation',
    ];

    protected const GZ_FORMAT = true;

    protected string $sqlDumperDir;

    protected mysqli $mysqli;

    protected SystemDir $systemDir;

    public function __construct(string $sqlDumperDir, mysqli $mysqli, SystemDir $systemDir)
    {
        $this->sqlDumperDir = implode(DIRECTORY_SEPARATOR, [$sqlDumperDir, date('m')]);
        $this->mysqli       = $mysqli;
        $this->systemDir    = $systemDir;

        $this->systemDir->createDir($this->sqlDumperDir);
    }

    /**
     * @param array<string> $tables
     * @param array<string> $forceExcludes
     */
    public function export(array $tables = [], array $forceExcludes = []): void
    {
        $excludes = array_unique(array_merge(self::DEFAULT_EXCLUDES, $forceExcludes));

        $webalizeString = '';
        if (count($tables) > 0) {
            $webalizeString = Strings::webalize(implode(';', $tables));
        }

        if (self::GZ_FORMAT) { // @phpstan-ignore-line
            $dumpFileName = sprintf('dump-%s-%s-%s.sql.gz', date('YmdHis'), $webalizeString, Random::generate());
        } else {
            $dumpFileName = sprintf('dump-%s-%s-%s.sql', date('YmdHis'), $webalizeString, Random::generate());
        }

        $dumpPath = implode(DIRECTORY_SEPARATOR, [$this->sqlDumperDir, $dumpFileName]);

        $dumper = new MySQLDump($this->mysqli);

        if (count($tables) > 0) {
            $dumper->tables['*'] = MySQLDump::NONE;

            foreach ($tables as $table) {
                if ($table === '*') {
                    $excludes = $forceExcludes;
                } elseif (in_array($table, $excludes, true)) {
                    unset($excludes[array_search($table, $excludes, true)]);
                }

                $dumper->tables[$table] = MySQLDump::ALL;
            }
        }

        foreach ($excludes as $table) {
            $dumper->tables[$table] = MySQLDump::NONE;
        }

        $dumper->save($dumpPath);
    }

    public function import(string $file): void
    {
        die('not now');
    }
}
